// app/controller/Picture.js

Ext.define('StoreTutorial.controller.Pictures', {

    extend: 'Ext.app.Controller',
    config:{
	 	refs: {
	 		theViewPort: 'viewport',
		    carouselView: 'carousel',
		    titleBar: 'carousel titlebar',
		    favoriteButton: 'titlebar #favoriteButton',
		    favoriteView: 'dataview'
		},   	
    },


    onActiveItemChange: function(carousel, item) {
    	var ps = Ext.getStore('Pictures');

    	// console.log('Number of pictures: ' + ps.getCount());

    	// console.log( carousel.getActiveIndex() );

    	// console.log(ps.getAt(carousel.getActiveIndex() + 1).get('title'))

		this.getTitleBar().setTitle(item.getPicture().get('title'));
		pressedCls = this.getFavoriteButton().getPressedCls(); 


		if(ps.getAt(carousel.getActiveIndex()) != undefined) {
			if(ps.getAt(carousel.getActiveIndex()).get('favorited') != 1) {
				this.getFavoriteButton().removeCls(pressedCls);
				this.getFavoriteButton().pressed = false;			
			} else {
				this.getFavoriteButton().addCls(pressedCls);
				this.getFavoriteButton().pressed = true;
			}			
		}

    },

    init: function() {

    	me = this;

	    this.control({
	        'carouselView': {
				activeitemchange: this.onActiveItemChange,
	        },

	        'favoriteButton': {
	        	tap: function(button) {
					var pressedCls = button.getPressedCls(),
					ps = Ext.getStore('Pictures'),
					fs = Ext.getStore('Favorites');
					

					var carousel = this.getCarouselView(),
					isPressed  = button.pressed,
					index  = carousel.getActiveIndex() +2;

					var myidx = ps.getById("ext-record-" + index);
					console.log("myidx:" + myidx.get('title'));


					if(isPressed) {
						button.removeCls(pressedCls);
						ps.getById("ext-record-" + index).set('favorited', 0);
					} else {
						button.addCls(pressedCls);
						ps.getById("ext-record-" + index).set('favorited', 1);
					}

					fs = me.cloneStore(ps, fs);

			       	fs.filter([
			       		{
			       			filterFn: function(item) {
			       				console.log(item.get("favorited"));
			       				return item.get("favorited") == 1; 
			       			}
			       		}
			      	]);					

					this.getFavoriteView().refresh();
                    button.pressed = !button.pressed;
                },	 
	        }
		});
    },
	    
    launch: function() {

    	var carousel = this.getCarouselView(), 
    				   titleVisible = false,
    				   infoView       = this.getTitleBar();

        /**
         * The Pictures store (see app/store/Pictures.js) is set to not load automatically, so we load it 
         * manually now. This loads data from the APOD RSS feed and calls our callback function once it's
         * loaded.
         * 
         * All we do here is iterate over all of the data, creating an apodimage Component for each item. 
         * Then we just add those items to the Carousel and set the first item active.
         */
        ps = Ext.getStore('Pictures'),
        fs = Ext.getStore('Favorites');


        me = this

        ps.load(function(pictures) {
            var items = [];
            
            Ext.each(pictures, function(picture) {
                if (!picture.get('image')) {
                    return;
                }
                
                items.push({
                    xtype: 'apodimage',
                    picture: picture
                });
            });




            carousel.setItems(items);
            carousel.setActiveItem(0);
-			console.log(fs.getAt(0));
        });

        /**
         * The final thing is to add a tap listener that is called whenever the user taps on the screen.
         * We do a quick check to make sure they're not tapping on the carousel indicators (tapping on
         * those indicators moves you between items so we don't want to override that), then either hide 
         * or show the info Component.
         * 
         * Note that to hide or show this Component we're adding or removing the apod-title-visible class.
         * If you look at index.html you'll see the CSS rules style the info bar and also cause it to fade
         * in and out when you tap.
         */  
        Ext.Viewport.element.on('tap', function(e) {
            if (e.getTarget('.x-img')) {
                if (titleVisible) {
                    infoView.element.removeCls('apod-title-visible');
                    titleVisible = false;
                } else {
                    infoView.element.addCls('apod-title-visible');
                    titleVisible = true;
                }
            }
        });



    },

	cloneStore: function(source, target) {
	    var records = source.getRange(0, source.getCount());
		console.log(records.length);

	   // target.suspendEvents(false);
	    target.removeAll();
	    target.clearFilter(true);
	    var flt = [];
	    for(var i=0; i<records.length; i++)
	    {
	    	flt.push(records[i].copy());

	    }
	            
	    target.add(flt);
	    target.resumeEvents();
	    return target;
	}    
});    