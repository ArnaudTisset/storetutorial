// app/store/Favorites.js

/**
 * Grabs the APOD RSS feed from Google's Feed API, passes the data to our Model to decode
 */
Ext.define('StoreTutorial.store.Favorites', {
    extend: 'Ext.data.Store',
    
    config: {
        model: 'StoreTutorial.model.Picture'
    }
});