Ext.define('StoreTutorial.view.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'main',
    requires: [
        'Ext.TitleBar','StoreTutorial.view.TiledDataView'
    ],
    config: {
        tabBarPosition: 'bottom',

        items: [
            {
                title: 'Welcome',
                iconCls: 'home',

                styleHtmlContent: true,
                scrollable: true,

                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Welcome to Sencha Touch 2'
                },

                html: [
                    "You've just generated a new Sencha Touch 2 project. What you're looking at right now is the ",
                    "contents of <a target='_blank' href=\"app/view/Main.js\">app/view/Main.js</a> - edit that file ",
                    "and refresh to change what's rendered here."
                ].join("")
            },
            {
                /*
                action
                add
                arrow_up
                arrow_right
                arrow_down
                arrow_left
                bookmarks
                compose
                delete
                download
                favorites
                info
                more
                refresh
                reply
                search
                settings
                star
                team
                time
                trash
                user
                */
                title: 'Pictures',

                iconCls: 'bookmarks',                
                // we define the carousel
                xtype: 'carousel',
                
                // giving an id to future references
                id: 'carouselView',
                store: 'Pictures',
                direction: 'horizontal',
                items:[{
                    /**
                     * This is just a reusable Component that we pin to the top of the page. This is hidden by default
                     * and appears when the user taps on the screen. The activeitemchange listener above updates the 
                     * content of this Component whenever a new image is swiped to
                     */                
                    xtype: 'titlebar',
                    id: 'infoView',
                    cls: 'apod-title',
                    top: 0,
                    left: 0,
                    right: 0,
                    //and now we define the items we want inside the title bar
                    items: [                 

                        {
                            xtype: 'button',
                            iconCls: 'favorites',
                            id: 'favoriteButton',

                           config : {
                                pressedCls : Ext.baseCSSPrefix + 'button-pressing'
                            },



                            // text: 'favorite',
                            iconMask: true,
                            ui: 'action',
                            scope: this,

                            // and then align to the center
                            align: 'center'
                        }
                    ]
                }]
            },
            {
                title: 'Favorite pictures',
                xtype: 'dataview', 
                store: 'Favorites',
                scrollable: true,
                inline: true,
                itemTpl: '<img src="{image}" width="200">',      
                layout: 'fit',

                iconCls: 'favorites'
            },        

        ]
    },
});

/**

                    items: [{
                        xtype: 'dataview',
                        scrollable: true,
                        inline: true,
                        itemTpl: '<img src="{image}">',
                        docked: 'top',
                    },
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Here are your favorites'
                    }],
*/