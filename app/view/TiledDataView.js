// Define our custom TitleDataView component
Ext.define('StoreTutorial.view.TiledDataView', {
    // Extend dataview
    extend: 'Ext.dataview.DataView',

    config: {
        // Give it a custom baseCls so we can target this view and its items
        baseCls: 'rd-tiled-view',

        // Give it a simple itemTpl which displays the field name within the item
        itemTpl: '{name}'
    }
});